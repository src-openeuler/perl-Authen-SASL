Name:           perl-Authen-SASL
Version:        2.1700
Release:        2
Summary:        Perl's SASL authentication framework
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Authen-SASL
Source0:        https://cpan.metacpan.org/authors/id/E/EH/EHUELS/Authen-SASL-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  coreutils findutils make perl-interpreter perl-generators
BuildRequires:  perl(:VERSION) >= 5.6 perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict) perl(warnings) sed perl(bytes) perl(Carp) perl(Digest::HMAC_MD5) perl(Digest::MD5)
BuildRequires:  perl(GSSAPI) perl(Tie::Handle) perl(vars) perl(FindBin) perl(Test::More)

%description
Authen::SASL::Perl is the pure Perl implementation of SASL mechanisms in the Authen::SASL framework,
At the time of this writing it provides the client part implementation for the following SASL mechanisms.

%package help
Summary:        Documentation for perl-Authen-SASL

%description help
Documentation for perl-Authen-SASL.

%prep
%autosetup -n Authen-SASL-%{version} -p1

chmod -c a-x example_pl

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -delete
%{__chmod} -Rf a+rX,u+w,g-w,o-w %{buildroot}

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc api.txt Changes example_pl README
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.1700-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Feb 06 2024 wangkai <13474090681@163.com> - 2.1700-1
- Update to 2.1700

* Tue Nov 26 2019 fengbing <fengbing7@huawei.com> - 2.16-16
- Package init
